
import requests
import json
from pdb import set_trace as pb

# https://www.nameapi.org/en/developer/downloads/
# https://juliensalinas.com/en/REST_API_fetching_go_golang_vs_python/
def format_name_helper(name, name_type = 'GIVENNAME'): #SURNAME

    url = (
        "http://api.nameapi.org/rest/v5.3/formatter/personnameformatter?"
        "apiKey=8cc1ef209866a18395f1ae0d67917b15-user1"
    )

    # Dict of data to be sent to NameAPI.org:
    payload = {
        "inputPerson": {
            "type": "NaturalInputPerson",
            "personName": {
                "nameFields": [
                    {
                        "string": name,
                        "fieldType": name_type
                    }
                ]
            },
            "gender": "UNKNOWN"
        }
    }

    # Proceed, only if no error:
    try:
        # Send request to NameAPI.org by doing the following:
        # - make a POST HTTP request
        # - encode the Python payload dict to JSON
        # - pass the JSON to request body
        # - set header's 'Content-Type' to 'application/json' instead of
        #   default 'multipart/form-data'
        resp = requests.post(url, json=payload)
        resp.raise_for_status()
        # Decode JSON response into a Python dict:
        resp_dict = resp.json()
        print(resp_dict)
    except requests.exceptions.HTTPError as e:
        print("Bad HTTP status code:", e)
    except requests.exceptions.RequestException as e:
        print("Network error:", e)
    name = resp_dict['formatted']
    return name











# https://forebears.io/onograph/documentation/api/location/batch
def name_nationality_count(names): #SURNAME
    # too expensive...
    url = (
        "https://ono.4b.rs/v1/jurs?"
        "key=b2be04f8b4c94b708a3a31fe63b6ceed"
    )

    # Dict of data to be sent to NameAPI.org:

    pb()
    names_json = names.to_json(orient='records')
    payload = names_json

    # Proceed, only if no error:
    try:
        # Send request to NameAPI.org by doing the following:
        # - make a POST HTTP request
        # - encode the Python payload dict to JSON
        # - pass the JSON to request body
        # - set header's 'Content-Type' to 'application/json' instead of
        #   default 'multipart/form-data'
        requests.post(url, data=json.dumps(dict(name='username'))).json()
        resp = requests.post(url, json={"name": "test"})
        resp.raise_for_status()
        # Decode JSON response into a Python dict:
        resp_dict = resp.json()
        print(resp_dict)
    except requests.exceptions.HTTPError as e:
        print("Bad HTTP status code:", e)
    except requests.exceptions.RequestException as e:
        print("Network error:", e)
    return name