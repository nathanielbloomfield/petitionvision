 
from pdb import set_trace as pb
from extract_petitions import extract_petitions
from build_sheet import build_worksheet_from_petition
import glob
import os


easy_petitions = glob.glob('input/*.pdf')

# dealing with images
img_petitions = glob.glob('input/*.jpeg')

# ===============================================================
if False:
    extract_petitions(easy_petitions,
                      table_folder='output/tables_only', table_quick_folder='output/tables_only_quick',
                      raw_images_folder='output/raw images',
                      spreadsheet_folder = 'output/spreadsheets', plot_intermediates = False,
                      table_dilate_size=9, table_canny_thresh1=10, table_canny_thresh2=100,
                      cell_dilate_size = 2, cell_canny_thresh1=10, cell_canny_thresh2=100,
                      cell_linethresh = 0, cell_minLineLength = 100, cell_maxLineGap = 3,
                      column_lines = 5, row_lines=15, landscape=True)

if False:
    extract_petitions(['input/Climate_Petition_-_NOT_YET_ENTERED_-_156.pdf-01.pdf-rotated.pdf'],
                      table_folder='output/tables_only', table_quick_folder='output/tables_only_quick',
                      raw_images_folder='output/raw images',
                      spreadsheet_folder = 'output/spreadsheets', plot_intermediates = False,
                      table_dilate_size=9, table_canny_thresh1=10, table_canny_thresh2=100,
                      cell_dilate_size = 2, cell_canny_thresh1=10, cell_canny_thresh2=100,
                      cell_linethresh = 0, cell_minLineLength = 100, cell_maxLineGap = 3,
                      column_lines = 5, row_lines=15, landscape=True)

if True:
    extract_petitions(img_petitions,
                      table_folder='output/tables_only', table_quick_folder='output/tables_only_quick',
                      raw_images_folder='output/raw images',
                      spreadsheet_folder = 'output/spreadsheets', plot_intermediates = False,
                      table_dilate_size=9, table_canny_thresh1=10, table_canny_thresh2=100,
                      cell_dilate_size = 2, cell_canny_thresh1=10, cell_canny_thresh2=100,
                      cell_linethresh = 0, cell_minLineLength = 100, cell_maxLineGap = 3,
                      column_lines = 5, row_lines=15, landscape=True)