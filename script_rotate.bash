 #!/bin/bash


 
FILES=individual/*.pdf
for f in $FILES
do
echo "Processing $f file..."
pdftk "$f" rotate 1-endsouth output "$f-rotated.pdf" 
mv "$f-rotated.pdf" "rotated/${s##*/}"
done

 
FILES=individual/Climate_Petition_-_NOT_YET_ENTERED_-_146.pdf-0*
for f in $FILES
do
echo "Processing $f file..."
pdftk "$f" rotate 1-endright output "$f-rotated.pdf" 
mv "$f-rotated.pdf" "rotated/${s##*/}"
done 
 
 
FILES=individual/Climate_Petition_-_NOT_YET_ENTERED_-_147.pdf-0*
for f in $FILES
do
echo "Processing $f file..."
pdftk "$f" rotate 1-endleft output "$f-rotated.pdf" 
mv "$f-rotated.pdf" "rotated/${s##*/}"
done 


 
FILES=individual/Climate_Petition_-_NOT_YET_ENTERED_-_157.pdf-0*
for f in $FILES
do
echo "Processing $f file..."
pdftk "$f" rotate 1-endright output "$f-rotated.pdf" 
mv "$f-rotated.pdf" "rotated/${s##*/}"
done 


FILES=individual/Climate_Petition_-_NOT_YET_ENTERED_-_156*
for f in $FILES
do
echo "Processing $f file..."
pdftk "$f" rotate 1-endeast output "$f-rotated.pdf" 
mv "$f-rotated.pdf" "rotated/${s##*/}"
done 


FILES=individual/Climate_Petition_-_NOT_YET_ENTERED_-_150*
for f in $FILES
do
echo "Processing $f file..."
pdftk "$f" rotate 1-endnorth output "$f-rotated.pdf" 
mv "$f-rotated.pdf" "rotated/${s##*/}"
done 

FILES=individual/Climate_Petition_-_NOT_YET_ENTERED_-_162*
for f in $FILES
do
echo "Processing $f file..."
pdftk "$f" rotate 1-endwest output "$f-rotated.pdf" 
mv "$f-rotated.pdf" "rotated/${s##*/}"
done 
