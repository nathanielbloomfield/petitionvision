
import json
import os
import sys
import requests
import time
from io import BytesIO

import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from PIL import Image
from pdb import set_trace as pb
import pandas as pd

def OCR_recognition(image_path, plotme=False):

    subscription_key = "7ef109bd7da14ccb8d4d2d8a72e42673"
    endpoint = 'https://testtest123141531.cognitiveservices.azure.com/'
    text_recognition_url = endpoint + "/vision/v3.0/read/analyze"
    image_data = open(image_path, "rb").read()

    headers = {'Ocp-Apim-Subscription-Key': subscription_key,'Content-Type': 'application/octet-stream'}
    params = {'visualFeatures': 'Categories,Description,Color'}
    response = requests.post(
        text_recognition_url, headers=headers, params=params, data=image_data)
    response.raise_for_status()

    analysis = {}
    poll = True
    while (poll):
        response_final = requests.get(
            response.headers["Operation-Location"], headers=headers)
        analysis = response_final.json()

        # print(json.dumps(analysis, indent=4))

        time.sleep(1)
        if ("analyzeResult" in analysis):
            poll = False
        if ("status" in analysis and analysis['status'] == 'failed'):
            poll = False

    polygons = []
    if ("analyzeResult" in analysis):
        # Extract the recognized text, with bounding boxes.
        polygons = [[(words["boundingBox"], words["text"])
                    for words in line['words']]
                    for line in analysis["analyzeResult"]["readResults"][0]["lines"]]
        polygons = [item for sublist in polygons for item in sublist]

    # Display the image and overlay it with the extracted text.
    if plotme:
        image = Image.open(BytesIO(image_data))
        ax = plt.imshow(image)
        for polygon in polygons:
            vertices = [(polygon[0][i], polygon[0][i + 1])
                        for i in range(0, len(polygon[0]), 2)]
            text = polygon[1]
            patch = Polygon(vertices, closed=True, fill=False, linewidth=2, color='y')
            ax.axes.add_patch(patch)
            plt.text(vertices[0][0], vertices[0][1], text, fontsize=20, va="top")
        plt.show()

    return polygons