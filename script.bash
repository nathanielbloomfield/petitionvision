 #!/bin/bash
FILES=*.pdf
for f in $FILES
do
echo "Processing $f file..."
pdftk "$f" burst output "individual/$f-%02d.pdf"
done
rename -f "s/ /_/g" individual/*
