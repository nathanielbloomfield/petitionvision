
import json
from pdb import set_trace as pb
import cv2 as cv
import numpy as np
from PIL import Image

# img = cv.imread('FudanPed00001_mask.png')
# pb()


def process_mask_data(data_dir='clean data/', out_dir='processed images/', out_dir_masks='processed masks/',
                      masks_file='clean data/via_project_24Jul2020_20h3m_json.json'):

    with open(masks_file) as json_file:
        data = json.load(json_file)

    for image in data:
        image_data = data[image]
        img = cv.imread(data_dir + image_data['filename'])
        max_regions = len(image_data['regions'])
        if max_regions > 0:
            cv.imwrite(out_dir + image_data['filename'], img)
            img_edges =img
            img_edges = cv.cvtColor(img_edges, cv.COLOR_BGR2GRAY)
            img_edges = cv.Canny(img_edges, 50, 150, apertureSize=3)
            ret, img_edges = cv.threshold(img_edges, 127, 255, cv.THRESH_BINARY)
            cv.imwrite('processed/edge_dir/' + image_data['filename'], img_edges)
            img[:] = (0, 0, 0)
            for region in range(len(image_data['regions'])):
                print(region)
                segment_x = image_data['regions'][region]['shape_attributes']['all_points_x']
                segment_y = image_data['regions'][region]['shape_attributes']['all_points_y']
                segment = np.column_stack((segment_x, segment_y))
                cv.fillPoly(img, [segment], (0, 0, (region+1)))

            im_pil = Image.fromarray(img).quantize(colors=max_regions+1, dither=0)
            # np.unique(np.asarray(im_pil))
            im_pil.save(out_dir_masks + image_data['filename'], "png")

