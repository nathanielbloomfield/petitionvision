 
from pdb import set_trace as pb
from extract_petitions import extract_petitions
from build_sheet import build_worksheet_from_petition
import glob
import os


easy_petitions = glob.glob('input/*.pdf')

# not pdf
#easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00072.jpg')

# not landscape
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00057.pdf')

# these are really low quality
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00080.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00081.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00082.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00083.pdf')

# upside down
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00087.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00088.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00090.pdf')

# dealing with iamges
img_petitions = glob.glob('input/*.jpg')
img_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00072.jpg')
img_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00082.pdf.jpg')
img_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00083.pdf.jpg')

# ===============================================================
# not landscape - extra rows
if False:
    extract_petitions(['input/Climate Petition - NOT YET ENTERED - 00057.pdf'],
                      table_folder='output/tables_only', raw_images_folder='output/raw images',
                      spreadsheet_folder = 'output/spreadsheets', plot_intermediates = False,
                      table_dilate_size=9, table_canny_thresh1=10, table_canny_thresh2=100,
                      cell_dilate_size = 2, cell_canny_thresh1=10, cell_canny_thresh2=100,
                      cell_linethresh = 0, cell_minLineLength = 100, cell_maxLineGap = 3,
                      column_lines = 5, row_lines=28, landscape=False)


# upside down but otherwise okay
if False:
    extract_petitions(['input/Climate Petition - NOT YET ENTERED - 00087.pdf',
                   'input/Climate Petition - NOT YET ENTERED - 00088.pdf',
                   'input/Climate Petition - NOT YET ENTERED - 00090.pdf'],
                  table_folder='output/tables_only', raw_images_folder='output/raw images',
                  spreadsheet_folder = 'output/spreadsheets', plot_intermediates = False,
                  table_dilate_size=9, table_canny_thresh1=10, table_canny_thresh2=100,
                  cell_dilate_size = 2, cell_canny_thresh1=10, cell_canny_thresh2=100,
                  cell_linethresh = 0, cell_minLineLength = 100, cell_maxLineGap = 3,
                  column_lines = 5, row_lines=15, landscape=True, upsidedown=True)

# low quality ones
if False:
    extract_petitions(img_petitions,
                          table_folder='output/tables_only', raw_images_folder='output/raw images',
                          spreadsheet_folder = 'output/spreadsheets', plot_intermediates = False,
                          table_dilate_size=9, table_canny_thresh1=10, table_canny_thresh2=100,
                          cell_dilate_size = 2, cell_canny_thresh1=10, cell_canny_thresh2=100,
                          cell_linethresh = 0, cell_minLineLength = 100, cell_maxLineGap = 3,
                          column_lines = 5, row_lines=14, landscape=True)

    extract_petitions(['input/Climate Petition - NOT YET ENTERED - 00082.pdf.jpg'],
                          table_folder='output/tables_only', raw_images_folder='output/raw images',
                          spreadsheet_folder = 'output/spreadsheets', plot_intermediates = False,
                          table_dilate_size=9, table_canny_thresh1=10, table_canny_thresh2=100,
                          cell_dilate_size = 2, cell_canny_thresh1=10, cell_canny_thresh2=100,
                          cell_linethresh = 0, cell_minLineLength = 100, cell_maxLineGap = 3,
                          column_lines = 5, row_lines=13, landscape=True)

    extract_petitions(['input/Climate Petition - NOT YET ENTERED - 00083.pdf.jpg'],
                          table_folder='output/tables_only', raw_images_folder='output/raw images',
                          spreadsheet_folder = 'output/spreadsheets', plot_intermediates = False,
                          table_dilate_size=9, table_canny_thresh1=10, table_canny_thresh2=100,
                          cell_dilate_size = 2, cell_canny_thresh1=10, cell_canny_thresh2=100,
                          cell_linethresh = 0, cell_minLineLength = 100, cell_maxLineGap = 3,
                          column_lines = 5, row_lines=5, landscape=True)
if True:
    extract_petitions(['input/Climate Petition - NOT YET ENTERED - 00072_mod.jpg'],
                      table_folder='output/tables_only', raw_images_folder='output/raw images',
                      spreadsheet_folder = 'output/spreadsheets', plot_intermediates = False,
                      table_dilate_size=9, table_canny_thresh1=10, table_canny_thresh2=100,
                      cell_dilate_size = 2, cell_canny_thresh1=10, cell_canny_thresh2=100,
                      cell_linethresh = 0, cell_minLineLength = 100, cell_maxLineGap = 3,
                      column_lines = 6, row_lines=15, landscape=True)
# ===============================================================
# ['raw petitions/Climate Petition - NOT YET ENTERED - 00073.pdf']
if False:
    extract_petitions(easy_petitions,
                      table_folder='output/tables_only', raw_images_folder='output/raw images',
                      spreadsheet_folder = 'output/spreadsheets', plot_intermediates = False,
                      table_dilate_size=9, table_canny_thresh1=10, table_canny_thresh2=100,
                      cell_dilate_size = 2, cell_canny_thresh1=10, cell_canny_thresh2=100,
                      cell_linethresh = 0, cell_minLineLength = 100, cell_maxLineGap = 3,
                      column_lines = 5, row_lines=15, landscape=True)

