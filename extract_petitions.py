
from extract_functions import load_image_from_pdf, detect_table_area, threshold_image_for_table_detection, \
    threshold_image_for_cell_detection, detect_cells
from build_sheet import build_worksheet_from_petition
from azure_call import OCR_recognition
import glob
import os
import cv2
from pdb import set_trace as pb

def extract_petitions(pdf_files, table_folder='tables_only', raw_images_folder='raw images',
                      table_quick_folder = 'output/tables_only_quick',
                      spreadsheet_folder = 'spreadsheets', plot_intermediates = False,
                      table_dilate_size=9, table_canny_thresh1=10, table_canny_thresh2=100,
                      cell_dilate_size = 2, cell_canny_thresh1=10, cell_canny_thresh2=100,
                      cell_linethresh = 100, cell_minLineLength = 100, cell_maxLineGap = 3,
                      column_lines = 5, row_lines=15, landscape=True, upsidedown=False):

    for pdf in pdf_files:
        print(pdf)
        pdf_file = os.path.basename(pdf)
        if not os.path.exists(raw_images_folder):
            os.mkdir(raw_images_folder)
        if not os.path.exists(table_folder):
            os.mkdir(table_folder)
        if not os.path.exists(spreadsheet_folder):
            os.mkdir(spreadsheet_folder)
        if not os.path.exists(table_quick_folder):
            os.mkdir(table_quick_folder)

        if pdf.endswith('.pdf'):
            load_image_from_pdf(pdf_file_path=pdf, input_images=raw_images_folder)
            img = cv2.imread(raw_images_folder + '/' + pdf_file+'.jpg')
        else:
            img = cv2.imread(pdf)

        if upsidedown:
            img = cv2.rotate(img, cv2.ROTATE_180)

        img_tablethresh = threshold_image_for_table_detection(img, dilate_size=table_dilate_size,
                                                canny_thresh1=table_canny_thresh1, canny_thresh2=table_canny_thresh2)

        img_tablecrop, img_tableshow = detect_table_area(img, img_tablethresh, landscape,
                            plot_intermediates=plot_intermediates)

        if landscape:
            if (img_tablecrop.shape[0] > img_tablecrop.shape[1]):
                img_tablecrop = cv2.rotate(img_tablecrop, cv2.ROTATE_90_COUNTERCLOCKWISE)
        else:
            if (img_tablecrop.shape[1] > img_tablecrop.shape[0]):
                img_tablecrop = cv2.rotate(img_tablecrop, cv2.ROTATE_90_CLOCKWISE)

        cv2.imwrite(table_folder + '/' + pdf_file+ '_tablecrop.jpg', img_tablecrop)
        cv2.imwrite(table_folder + '/' + pdf_file+ '_tablecrop_show.jpg', img_tableshow)
        cv2.imwrite(table_folder + '/' + pdf_file+ '_tablecrop_thresh.jpg', img_tablethresh)

        img_cellthresh = threshold_image_for_cell_detection(img_tablecrop, canny_thresh1=cell_canny_thresh1,
                                                    canny_thresh2=cell_canny_thresh2, dilate_size=cell_dilate_size)

        rectangle_x, rectangle_y, rectangle_img, img_lines, img_raw_lines, img_r = detect_cells(img_tablecrop, img_cellthresh,
                     column_lines=column_lines, row_lines=row_lines, plot_intermediates=plot_intermediates,
                     line_thresh=cell_linethresh, minLineLength=cell_minLineLength, maxLineGap=cell_maxLineGap)

        cv2.imwrite(table_folder + '/' + pdf_file+ '_cell_thresh.jpg', img_cellthresh)
        cv2.imwrite(table_folder + '/' + pdf_file+ '_cell_res.jpg', img_lines)
        cv2.imwrite(table_quick_folder + '/' + pdf_file+ '_cell_res.jpg', img_lines)
        cv2.imwrite(table_folder + '/' + pdf_file+ '_cell_lines_raw.jpg', img_raw_lines)
        cv2.imwrite(table_folder + '/' + pdf_file+ '_cell_rotated.jpg', img_r)

        polygons = OCR_recognition(table_folder + '/' + pdf_file+ '_cell_rotated.jpg', plotme=False)

        build_worksheet_from_petition(rectangle_x, rectangle_y, rectangle_img, polygons,
                                  spreadsheet_name = spreadsheet_folder + '/' + pdf_file + '.xlsx')

