
import pandas as pd
import xlsxwriter
import cv2
import os
from pdb import set_trace as pb
import numpy as np


def build_worksheet_from_petition(rectangle_x, rectangle_y, rectangle_img, polygons,
                                  spreadsheet_name = 'Expenses01.xlsx'):

    if not os.path.exists('output/build_sheet'):
        os.mkdir('output/build_sheet')

    rectangle_img_names = []
    for i in range(len(rectangle_img)):
        rectangle_img_names.append('output/build_sheet/cut'+str(i)+'.jpg')
        cv2.imwrite(rectangle_img_names[i], rectangle_img[i])

    rectangle_x_fact = pd.factorize(rectangle_x, sort=True)[0]
    rectangle_x_levels = pd.factorize(rectangle_x, sort=True)[1]
    rectangle_y_fact = pd.factorize(rectangle_y, sort=True)[0]
    rectangle_y_levels = pd.factorize(rectangle_y, sort=True)[1]

    rectangle_y_fact = rectangle_y_fact*2

    workbook = xlsxwriter.Workbook(spreadsheet_name)
    worksheet = workbook.add_worksheet()

    for i in range(len(rectangle_img_names)):
        width = rectangle_img[i].shape[1]/2*0.141
        height = rectangle_img[i].shape[0]/2*0.8
        worksheet.set_column(rectangle_x_fact[i], rectangle_x_fact[i], width)
        worksheet.set_row(rectangle_y_fact[i], height)

    for i in range(len(rectangle_img_names)):
        worksheet.insert_image(rectangle_y_fact[i], rectangle_x_fact[i], rectangle_img_names[i], {'x_scale': 0.5, 'y_scale': 0.5})

    write_dict = {}
    for i in range(len(polygons)):
        polygon = polygons[i]
        mean_x = np.mean(polygon[0][::2])
        mean_y = np.mean(polygon[0][1::2])
        cell_x = np.max(np.argwhere(rectangle_x_levels < mean_x))
        cell_y = np.max(np.argwhere(rectangle_y_levels < mean_y))
        key = str(cell_x)+'-'+str(cell_y)
        if key in write_dict.keys():
            write_dict[key] = write_dict[key] + polygon[1]
        else:
            write_dict[key] = polygon[1]

    format = workbook.add_format()
    format.set_font_size(32)
    for key, value in write_dict.items():
        x = key.split("-")
        worksheet.write_string(int(x[1])*2+1, int(x[0]),  value, format)

    workbook.close()