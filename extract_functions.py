import cv2
from pdf2image import convert_from_path
import pytesseract
from pdb import set_trace as pb
from PIL import Image
import re
import os
import numpy as np
import imutils
from sklearn.cluster import KMeans

# ====================================================================================================
# ====================================================================================================

def load_image_from_pdf(pdf_file_path='Climate Petition - NOT YET ENTERED - 00111.pdf',
                        input_images='raw images'):
    pdf_file = os.path.basename(pdf_file_path)

    pdf_input = pdf_file_path
    pdf_raw_image = input_images+'/'+pdf_file + '.jpg'

    pages = convert_from_path(pdf_input, 500)
    for page in pages:
        page.save(pdf_raw_image, 'JPEG')

# ====================================================================================================
# ====================================================================================================

def threshold_image_for_table_detection(img, dilate_size=9, canny_thresh1=10, canny_thresh2=100):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # gray = cv2.GaussianBlur(gray,(3,3),0)
    edges = cv2.Canny(gray,canny_thresh1,canny_thresh2,apertureSize = 3)
    kernel = np.ones((dilate_size, dilate_size), np.uint8)
    edges = cv2.dilate(edges, kernel, iterations=1)

    thresh = edges
    return thresh

def threshold_image_for_cell_detection(img, canny_thresh1=10, canny_thresh2=100, dilate_size=2):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray, canny_thresh1, canny_thresh2, apertureSize=3)
    kernel = np.ones((dilate_size, dilate_size), np.uint8)
    edges = cv2.dilate(edges, kernel, iterations=1)
    return edges

# ====================================================================================================
# ====================================================================================================

def detect_table_area(img, thresh, landscape, plot_intermediates=False):
    # find contours
    mask = np.ones(img.shape[:2], dtype="uint8") * 255
    contours = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]

    contour_area = []
    for c in contours:
        # get the bounding rect
        rect = cv2.minAreaRect(c)
        box = np.int0(cv2.boxPoints(rect))
        contour_area.append(cv2.contourArea(box))
        cv2.drawContours(mask, [box], 0, (0, 0, 0), -1)  # OR

    res_final = cv2.bitwise_and(img, img, mask=cv2.bitwise_not(mask))
    if plot_intermediates:
        cv2.imwrite('mask.jpg', mask)
        cv2.imwrite('img.jpg', res_final)

    biggest_rectangle = np.argmax(contour_area)
    rect = cv2.minAreaRect(contours[biggest_rectangle])
    img_crop, img_rot = rotate_and_crop(img, rect, landscape)

    if plot_intermediates:
        cv2.imwrite('table_full_image.jpg', res_final)
        cv2.imwrite('rotated_image.jpg', img_rot)

    return img_crop, res_final


def rotate_and_crop(img, rect, landscape):

    if landscape:
        if abs(img.shape[2] - rect[1][1]) >= abs(img.shape[2] - rect[1][0]):
            rect_elongated = (rect[0], (rect[1][0], rect[1][1]*2), rect[2])
        else:
            rect_elongated = (rect[0], (rect[1][0]*2, rect[1][1]), rect[2])
    else:
        if abs(img.shape[2] - rect[1][1]) <= abs(img.shape[2] - rect[1][0]):
            rect_elongated = (rect[0], (rect[1][0], rect[1][1]*2), rect[2])
        else:
            rect_elongated = (rect[0], (rect[1][0]*2, rect[1][1]), rect[2])
    # rect_elongated = rect
    # turn other areas of image blank
    box = np.int0(cv2.boxPoints(rect_elongated))
    mask = np.ones(img.shape[:2], dtype="uint8") * 255
    cv2.drawContours(mask, [box], 0, 0, -1)  # OR
    # cv2.imwrite('actual_mask.jpg', mask)
    mask = mask == 255
    res_final = img
    res_final[mask] = (255, 255, 255)

    # get the parameter of the small rectangle
    angle = rect[2]

    img_rot = rotate_bound(res_final, -angle)
    thresh = threshold_image_for_table_detection(img_rot)

    coords = cv2.findNonZero(thresh)
    x, y, w, h = cv2.boundingRect(coords)  # Find minimum spanning bounding box
    img_crop = img_rot[y:y + h, x:x + w]
    return img_crop, img_rot



def rotate_bound(image, angle, background=255):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w / 2, h / 2)

    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH), borderValue=(background,background,background))


# ====================================================================================================
# ====================================================================================================

def find_lines_in_image(img, edges, line_thresh, minLineLength, maxLineGap):
    lines = cv2.HoughLinesP(image=edges, rho=2, theta=np.pi / 360, threshold=line_thresh, lines=np.array([]),
                            minLineLength=minLineLength, maxLineGap=maxLineGap)

    a, b, c = lines.shape

    img_raw_lines = img.copy()
    for i in range(a):
         cv2.line(img_raw_lines, (lines[i][0][0], lines[i][0][1]), (lines[i][0][2], lines[i][0][3]), (0, 0, 255), 3, cv2.LINE_AA)

    # get line gradients and intercepts
    lines_grad = []
    lines_angle = []
    lines_xintercept = []
    lines_yintercept = []
    lines_y1 = []
    lines_y2 = []
    lines_x1 = []
    lines_x2 = []
    for i in range(a):
        # y = m x + b
        max_x = np.argmax((lines[i][0][0], lines[i][0][2]))
        x1 = min(lines[i][0][0], lines[i][0][2])
        x2 = max(lines[i][0][0], lines[i][0][2])
        if (max_x == 1):
            y1 = lines[i][0][1]
            y2 = lines[i][0][3]
        else:
            y1 = lines[i][0][3]
            y2 = lines[i][0][1]

        dx = x2 - x1
        dy = y2 - y1
        if (x1 == x2):
            xintercept = x1
            yintercept = np.inf
            gradient = np.inf
            theta = 90
        elif (y1 == y2):
            xintercept = np.inf
            yintercept = y1
            gradient = 0
            theta = 0
        else:
            gradient = (y2 - y1) / (x2 - x1)
            yintercept = y1 - x1 * gradient
            xintercept = -yintercept/gradient
            theta = np.arctan(dy/(dx+0.0001))
            theta = theta * 180/np.pi

        lines_grad.append(gradient)
        lines_angle.append(theta)
        lines_xintercept.append(xintercept)
        lines_yintercept.append(yintercept)
        lines_y1.append(y1)
        lines_y2.append(y2)
        lines_x1.append(x1)
        lines_x2.append(x2)

    lines_grad = np.array(lines_grad)
    lines_angle = np.array(lines_angle)
    lines_xintercept = np.array(lines_xintercept)
    lines_yintercept = np.array(lines_yintercept)
    lines_y1 = np.array(lines_y1)
    lines_y2 = np.array(lines_y2)
    lines_x1 = np.array(lines_x1)
    lines_x2 = np.array(lines_x2)

    # find which lines are horizontal and vertical
    horizontal_lines = np.argwhere(abs(lines_angle) < 20)
    vertical_lines = np.argwhere(abs(lines_angle) > 70)

    # rotate_table_vert = np.mean(lines_angle[vertical_lines])-90
    rotate_table_hori = np.mean(lines_angle[horizontal_lines])
    rotate_total = rotate_table_hori

    img_lines_hori = img.copy()
    for line in lines[horizontal_lines]:
        cv2.line(img_lines_hori, (line[0][0][0], line[0][0][1]), (line[0][0][2], line[0][0][3]), (255, 0, 0), 5)
    img_lines_vert = img.copy()
    for line in lines[vertical_lines]:
        cv2.line(img_lines_vert, (line[0][0][0], line[0][0][1]), (line[0][0][2], line[0][0][3]), (255, 0, 0), 5)
    #cv2.imwrite('1horizontal.png', img_lines_hori)
    #cv2.imwrite('1vert.png', img_lines_vert)

    return rotate_total, lines_xintercept, lines_yintercept, lines_grad, \
                    lines_angle, horizontal_lines, vertical_lines, img_raw_lines, \
                    lines_x1, lines_x2, lines_y1, lines_y2


def detect_cells(img, edges,
                 column_lines=5, row_lines=15, plot_intermediates=False,
                 line_thresh=100, minLineLength=100, maxLineGap=3):

    rotate_total, lines_xintercept, lines_yintercept, lines_grad, lines_angle, horizontal_lines, \
        vertical_lines, img_raw_lines, lines_x1, lines_x2, lines_y1, lines_y2 \
         = find_lines_in_image(img, edges, line_thresh, minLineLength, maxLineGap)

    rotate_amount = rotate_total

    edges_r = rotate_bound(edges, -rotate_amount, background=0)
    img_r = rotate_bound(img, -rotate_amount, background=0)

    rotate_total, lines_xintercept, lines_yintercept, lines_grad, lines_angle, horizontal_lines, \
        vertical_lines, img_raw_lines, lines_x1, lines_x2, lines_y1, lines_y2, \
         = find_lines_in_image(img_r, edges_r, line_thresh, minLineLength, maxLineGap)


    # use kmeans clustering to find aggregate line positions
    try:
        min_vert = 0
        max_vert = img.shape[1]
        buffer = 40

        vertical_int = np.vstack((lines_x1[vertical_lines], lines_x2[vertical_lines]))
        # remove points from the start or end...
        vertical_int = vertical_int[vertical_int > (min_vert + buffer), np.newaxis]
        vertical_int = vertical_int[vertical_int < (max_vert - buffer), np.newaxis]

        kmeans_cols = KMeans(n_clusters=column_lines, random_state=0).fit(vertical_int)

        column_locations_c = kmeans_cols.cluster_centers_

        clusters_vert = kmeans_cols.predict(vertical_int)
        column_locations = []
        for cluster in np.unique(clusters_vert):
            column_locations.append(np.median(vertical_int[clusters_vert == cluster]))
        column_locations = np.array([column_locations]).transpose()

        horizontal_int = np.vstack((lines_y1[horizontal_lines], lines_y2[horizontal_lines]))
        kmeans_rows = KMeans(n_clusters=row_lines, random_state=0).fit(horizontal_int)

        row_locations_c = kmeans_rows.cluster_centers_

        clusters_hori = kmeans_rows.predict(horizontal_int)
        row_locations = []
        for cluster in np.unique(clusters_hori):
            row_locations.append(np.median(horizontal_int[clusters_hori == cluster]))
        row_locations = np.array([row_locations]).transpose()

    except:
        column_locations = [x for x in range(column_lines)]
        row_locations = [x for x in range(row_lines)]

    img_lines = img_r.copy()
    for i in range(len(column_locations)):
        cv2.line(img_lines, (column_locations[i], 0), (column_locations[i], img_r.shape[1]), (0, 0, 255), 3, cv2.LINE_AA)
    for i in range(len(row_locations)):
        cv2.line(img_lines, (0, row_locations[i]), (img_r.shape[1], row_locations[i]), (0, 0, 255), 3, cv2.LINE_AA)

    if plot_intermediates:
        cv2.imwrite('img_columns.jpg', img_lines)

    # build dummy image with clean cells
    mask = np.ones(img_r.shape[:2], dtype="uint8") * 255
    for i in range(len(column_locations)):
        cv2.line(mask, (column_locations[i], 0), (column_locations[i], img_r.shape[1]), 0, 1)
    for i in range(len(row_locations)):
        cv2.line(mask, (0, row_locations[i]), (img_r.shape[1], row_locations[i]), 0, 1)
    if plot_intermediates:
        cv2.imwrite('tablemask.jpg', mask)

    # extract cells as contours
    contours = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
    rectangle_x = []
    rectangle_y = []
    rectangle_img = []

    # cut out images of cells
    for cnt in contours:
        x,y,w,h = cv2.boundingRect(cnt)
        roi = img_r[y:y + h, x:x + w]
        rectangle_x.append(x)
        rectangle_y.append(y)
        rectangle_img.append(roi)

    return rectangle_x, rectangle_y, rectangle_img, img_lines, img_raw_lines, img_r
