
import re
from pdb import set_trace as pb
import glob
import pandas as pd
import numpy as np
import os
import re
from email_validator import validate_email, EmailNotValidError
from names_dataset import NameDataset
from name_api_helpers import format_name_helper, name_nationality_count

all_files = glob.glob('finalized_relabelled/DONE batch1/*.xlsx')

frames = []
for x in all_files:
    print(x)
    df = pd.read_excel(x)
    df = df.dropna(how='all')
    df = df.replace(np.nan, '', regex=True)
    df = df.reset_index()
    df[['index']] = os.path.basename(x)
    df[['index']] = os.path.basename(x)
    df.columns = ['Sheet', 'Firstname', 'Lastname', 'Postcode', 'Phone', 'Email', 'Signature']
    print(df.loc[0,:])
    df = df.drop(df.index[0])
    frames.append(df)

resdf=pd.concat(frames)
#resdf.to_csv('finalized_relabelled/batch1.csv')

# =============================================
# data checking
legitphonelengths = [0, 8, 10]
resdf['Phone']= resdf['Phone'].astype(str)
resdf["Phone.count"] = resdf['Phone'].str.len()
print(resdf.loc[~resdf['Phone.count'].isin(legitphonelengths)])

legitpostcodelengths = [0, 4]
resdf['Postcode']= resdf['Postcode'].astype(str)
resdf["Postcode.count"] = resdf['Postcode'].str.len()
print(resdf.loc[~resdf['Postcode.count'].isin(legitpostcodelengths)])

print ('================================')
print('add area codes to phone numbers')
print ('================================')
# get first digit of postcode, and match to phonenumber if eight digits in length
# https://info.australia.gov.au/about-australia/facts-and-figures/telephone-country-and-area-codes
# https://simple.wikipedia.org/wiki/Postcodes_in_Australia
phone_zip_dict = {
  "2": "02", #NSW+ACT
  "3": '03', #VIC
  "4": '07', #QLD
  "5": '08', #SA
  "6": '08', #WA
  "7": '03', #TAS
  "08": '08', #NT
}
resdf.reset_index(inplace=True)
for index, row in resdf.iterrows():
    if (row["Phone.count"] == 8 and row['Postcode.count'] == 4):
        zip_code_match = list(phone_zip_dict.keys())
        zip_code_index = np.where([row['Postcode'].startswith(x) for x in zip_code_match])[0][0]
        resdf.at[index, 'Phone'] = phone_zip_dict[zip_code_match[zip_code_index]] + row['Phone']

print ('================================')
print('checking email')
print ('================================')

resdf['Email.domain.valid'] = 'untested'
for index, row in resdf.iterrows():
    if (row['Email'] != ''):
        try:
            # Validate.
            valid = validate_email(row["Email"])
            # Update with the normalized form.
            email = valid.email
            resdf.at[index, 'Email.domain.valid'] = 'Yes'
        except EmailNotValidError as e:
            # email is not valid, exception message is human-readable
            print(index, row)
            print(str(e))
            print('================================')
            resdf.at[index, 'Email.domain.valid'] = 'No'

print ('================================')
print('checking names')
print ('================================')
m = NameDataset()
last_names = set(line.strip() for line in open('all.txt'))
last_names_cap = [x.capitalize() for x in last_names]

# have a look at forebears API - this might be quite useful???
# expensive and might not be so useful... if I could get the electoral roll though, that'd be good.
# names_df_last = pd.DataFrame(resdf[["Lastname"]])
# names_df_last.rename(columns={'Lastname': 'name'}, inplace=True)
# names_df_last['type'] = 'forename'
# names_df_last['limit'] = 10
# names_df_last['sanitize'] = 0
# names_df_last.reset_index(inplace=True)
# names_df_last['id'] = names_df_last.index+1
#
# name_nationality_count(names_df_last)
#
# names_df_last = pd.DataFrame(resdf[['Firstname']])
# names_df_last.rename(columns={'Firstname': 'name'}, inplace=True)
# names_df_last['type'] = 'forename'
# names_df_last['limit'] = 10
# names_df_last['sanitize'] = 0
# names_df_last.reset_index(inplace=True)
# names_df_last['id'] = names_df_last.index+1
#
# name_nationality_count(names_df_last)

for index, row in resdf.iterrows():

    # Fix up capitalisation
    first_lower_count = sum(map(str.islower, row['Firstname']))
    first_upper_count = sum(map(str.isupper, row['Firstname']))
    first_count  = sum(map(str.islower, row['Firstname'].lower()))
    if (first_count == first_lower_count or first_count == first_upper_count):
        resdf.at[index, 'Firstname'] = format_name_helper(row['Firstname'], name_type='GIVENNAME')

    first_lower_count = sum(map(str.islower, row['Lastname']))
    first_upper_count = sum(map(str.isupper, row['Lastname']))
    first_count = sum(map(str.islower, row['Lastname'].lower()))
    if (first_count == first_lower_count or first_count == first_upper_count):
        resdf.at[index, 'Lastname'] = format_name_helper(row['Lastname'], name_type='SURNAME')

    # find number of people with this name
    # firstname = row['Firstname'].strip().lower() in m.first_names
    # lastname = row['Lastname'].strip().lower() in m.last_names or row['Lastname'].strip().capitalize() in last_names_cap
    # if (firstname and lastname):
    #     #pb()
    #     pass
    # else:
    #     print('name potentially not valid')
    #     print(row)

# ==================================================================

full_data = resdf[(resdf['Phone'] != '') & (resdf['Postcode'] != '') & (resdf['Email'] != '')]
part_data = resdf[(resdf['Phone'] == '') | (resdf['Postcode'] == '') | (resdf['Email'] == '')]

full_data.to_csv('finalized_relabelled/batch1_full.csv')
part_data.to_csv('finalized_relabelled/batch1_part.csv')








