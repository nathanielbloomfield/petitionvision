 
from pdb import set_trace as pb
from extract_petitions import extract_petitions
from build_sheet import build_worksheet_from_petition
import glob
import os


easy_petitions = glob.glob('input/*.pdf')

# these are really low quality
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00113.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00117.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00140.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00143.pdf')

# upside down
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00114.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00115.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00116.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00120.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00121.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00122.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00123.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00124.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00125.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00126.pdf')
easy_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00128.pdf')

# dealing with images
img_petitions = glob.glob('input/*.jpg')
img_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00118.jpg')
img_petitions.remove('input/Climate Petition - NOT YET ENTERED - 00119.jpg')

# ===============================================================
if False:
    extract_petitions(easy_petitions,
                      table_folder='output/tables_only', table_quick_folder='output/tables_only_quick',
                      raw_images_folder='output/raw images',
                      spreadsheet_folder = 'output/spreadsheets', plot_intermediates = False,
                      table_dilate_size=9, table_canny_thresh1=10, table_canny_thresh2=100,
                      cell_dilate_size = 2, cell_canny_thresh1=10, cell_canny_thresh2=100,
                      cell_linethresh = 0, cell_minLineLength = 100, cell_maxLineGap = 3,
                      column_lines = 5, row_lines=15, landscape=True)

# cleaned up messy ones
if False:
    extract_petitions(img_petitions,
                      table_folder='output/tables_only', table_quick_folder='output/tables_only_quick',
                      raw_images_folder='output/raw images',
                      spreadsheet_folder = 'output/spreadsheets', plot_intermediates = False,
                      table_dilate_size=9, table_canny_thresh1=10, table_canny_thresh2=100,
                      cell_dilate_size = 2, cell_canny_thresh1=10, cell_canny_thresh2=100,
                      cell_linethresh = 0, cell_minLineLength = 100, cell_maxLineGap = 3,
                      column_lines = 5, row_lines=15, landscape=True)

# upside down but otherwise okay
if True:
    extract_petitions(['input/Climate Petition - NOT YET ENTERED - 00114.pdf',
                   'input/Climate Petition - NOT YET ENTERED - 00115.pdf',
                   'input/Climate Petition - NOT YET ENTERED - 00116.pdf',
                   'input/Climate Petition - NOT YET ENTERED - 00120.pdf',
                   'input/Climate Petition - NOT YET ENTERED - 00121.pdf',
                   'input/Climate Petition - NOT YET ENTERED - 00122.pdf',
                   'input/Climate Petition - NOT YET ENTERED - 00123.pdf',
                   'input/Climate Petition - NOT YET ENTERED - 00124.pdf',
                   'input/Climate Petition - NOT YET ENTERED - 00125.pdf',
                   'input/Climate Petition - NOT YET ENTERED - 00126.pdf',
                   'input/Climate Petition - NOT YET ENTERED - 00128.pdf'],
                      table_folder='output/tables_only', table_quick_folder='output/tables_only_quick',
                      raw_images_folder='output/raw images',
                      spreadsheet_folder = 'output/spreadsheets', plot_intermediates = False,
                      table_dilate_size=9, table_canny_thresh1=10, table_canny_thresh2=100,
                      cell_dilate_size = 2, cell_canny_thresh1=10, cell_canny_thresh2=100,
                      cell_linethresh = 0, cell_minLineLength = 100, cell_maxLineGap = 3,
                      column_lines = 5, row_lines=15, landscape=True, upsidedown=True)
