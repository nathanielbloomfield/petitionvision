
import mechanize
import pandas as pd
from pdb import set_trace as pb


res = pd.read_csv('finalized_relabelled/batch1_full.csv', dtype=str)

for index, row in res.iterrows():
    print('----------------------------------------------------------')
    print(row)
    br = mechanize.Browser()
    br.open("https://greens.org.au/campaigns/climate-emergency")
    br.select_form(nr=0)
    br.form['first_name'] = row['Firstname']
    br.form['last_name'] = row['Lastname']
    br.form['email_address'] = row['Email']
    br.form['phone_number'] = row['Phone']
    br.form['postcode'] = row['Postcode']
    print(br.form)
    br.submit()

